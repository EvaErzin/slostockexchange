from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from bokeh.plotting import figure
from bokeh.embed import components
from datetime import datetime, timedelta
from .forms import *

from .models import *

def index(request):
    template = loader.get_template('index.html')
    return HttpResponse(template.render())

def nakupi(request):
    template = loader.get_template('nakupi.html')
    naki = Nakup.objects.all()
    slovar = {}
    for nak in naki:
        if nak.simbol not in slovar.keys():
            slovar[nak.simbol] = nak.number
        else:
            slovar[nak.simbol] += nak.number
    context = {
        'nakupi': list(slovar.items())
    }
    return HttpResponse(template.render(context, request))

def nakup(request, id):
    template = loader.get_template('nakup.html')
    n = Nakup.objects.filter(simbol=id)
    povp = 0
    st = 0
    for nak in n:
        povp += nak.value * nak.number
        st += nak.number
    povp /= st
    deln = Delnica.objects.get(simbol=id)
    zgodovina = deln.history.all().order_by('date')
    dat = [d.date.isoformat() for d in zgodovina]
    vr = [d.closeValue for d in zgodovina]
    p = [povp for i in range(len(dat))]
    plot = figure(plot_height=400, plot_width=600, x_range=dat)
    plot.circle(dat, vr, fill_color='white', line_color='black', size=8, legend='Uradni tečaj')
    plot.circle(dat, povp, fill_color='white', line_color='firebrick', size=8, legend='Povprečna vrednost kupljenih delnic')
    plot.line(dat, vr, line_width=3, color='black', legend='Uradni tečaj')
    plot.line(dat, p, line_width=3, color='firebrick', legend='Povprečna vrednost kupljenih delnic')
    plot.legend.background_fill_alpha = 0.95
    plot.legend.location = 'top_left'
    script, div = components(plot)
    context = {
        'scr1': script,
        'div1': div,
        'nakupi': Nakup.objects.filter(simbol=id),
        'delnica': Delnica.objects.get(simbol=id),
        'povp': "{0:.2f}".format(povp),
        'st': st,
        'skupno': "{0:.2f}".format(povp * st),
        'trVr': "{0:.2f}".format(deln.closeValue * st)
    }
    return HttpResponse(template.render(context, request))

def delnice(request):
    template = loader.get_template('delnice.html')
    context = {
        'delnice': Delnica.objects.all().order_by('-closeValue')
    }
    return HttpResponse(template.render(context, request))


def delnica(request, id):
    template = loader.get_template('delnica.html')
    deln = Delnica.objects.get(simbol=id)
    zgodovina = deln.history.all().order_by('date')
    dat = [d.date.isoformat() for d in zgodovina]
    vr = [d.closeValue for d in zgodovina]
    plot = figure(plot_height=400, plot_width=600, x_range=dat)
    plot.circle(dat, vr, fill_color='white', line_color='firebrick', size=8)
    plot.line(dat, vr, line_width=2, color='firebrick')
    plot.legend.background_fill_alpha = 0
    plot.legend.location = 'top_left'
    script, div = components(plot)
    context = {
        'scr1': script,
        'div1': div,
        'delnica': Delnica.objects.get(simbol=id),
        'zgodovina': zgodovina
    }
    return HttpResponse(template.render(context, request))

def nov_nakup(request):
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = NakupForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            nak = Nakup()
            form.save()
            return HttpResponseRedirect('/nov_nakup/')

    else:
        form = NakupForm()

    return render(request, 'nov_nakup.html', {'form': form})
