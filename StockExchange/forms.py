from django.forms import ModelForm
from .models import Nakup

class NakupForm(ModelForm):

    class Meta:
        model = Nakup
        fields = '__all__'