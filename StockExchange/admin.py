from django.contrib import admin
from simple_history.admin import SimpleHistoryAdmin

from .models import Delnica, Nakup

admin.site.register(Delnica, SimpleHistoryAdmin)
admin.site.register(Nakup)


# Register your models here.
