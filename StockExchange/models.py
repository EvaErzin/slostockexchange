from django.db import models
from simple_history.models import HistoricalRecords
from datetime import datetime

class Delnica(models.Model):
    simbol = models.CharField(max_length=6, primary_key=True, verbose_name='Simbol')
    date = models.DateField(verbose_name='Datum')
    closeValue = models.FloatField(verbose_name='Uradna vrednost')
    history = HistoricalRecords()
    _history_date = None

    @property
    def _history_date(self):
        return self.__history_date

    @_history_date.setter
    def _history_date(self, value):
        self.__history_date = value

    def __str__(self):
        return self.simbol

    class Meta:
        verbose_name_plural = 'Delnice'

class Nakup(models.Model):
    simbol = models.ForeignKey(Delnica, verbose_name='Simbol')
    number = models.IntegerField(verbose_name='Število')
    date = models.DateField(verbose_name='Datum nakupa')
    value = models.FloatField(verbose_name='Cena delnice')

    class Meta:
        verbose_name_plural = 'Nakupi'


