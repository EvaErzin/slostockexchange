from django.conf.urls import url, include
from . import views

urlpatterns = [
    url(r'^$', views.index, name='Index'),
    url(r'^delnice/$', views.delnice, name='Delnice'),
    url(r'^delnice/(?P<id>.+)$', views.delnica, name='Delnica'),
    url(r'^nakupi/$', views.nakupi, name='Nakupi'),
    url(r'^nakupi/(?P<id>.+)$', views.nakup, name='Nakup'),
    url(r'^nov_nakup/', views.nov_nakup, name='Nov Nakup')
]