from django.core.management.base import BaseCommand, CommandError
from urllib.request import urlretrieve
from datetime import datetime, date
from django.utils import timezone
from StockExchange.models import *


class Command(BaseCommand):

    def handle(self, *args, **options):
        url = 'http://www.ljse.si/datoteke/BTStecajEUR.txt'
        fileName = 'tecaj_{}.txt'.format(date.today())

        urlretrieve(url, fileName)

        # Delnica.objects.all().delete()
        # Delnica.history.all().delete()

        def makeDelnice(file_name):
            datum = None
            with open(file_name) as file:
                for line in file:
                    code = line[1:5].lstrip().rstrip()
                    if code == '0002':
                        dat = line[6:16].rstrip().lstrip().split('.')
                        dan = int(dat[0])
                        mesec = int(dat[1])
                        leto = int(dat[2])
                        datum = timezone.make_aware(datetime(day=dan, month=mesec, year=leto))
                    if code == '0010':
                        symbol = line[6:14].lstrip().rstrip()
                        value = line[56:71].lstrip().rstrip().replace(',', '.')
                        if value == '': continue
                        value = float(value)
                        try:
                            delnica = Delnica.objects.get(simbol=symbol)
                            if delnica.date == datum.date():
                                continue
                            delnica.date = datum
                            delnica.closeValue = value
                            delnica._history_date = datum
                            delnica.save()
                        except Delnica.DoesNotExist:
                            delnica = Delnica(simbol=symbol)
                            delnica.date = datum
                            delnica._history_date = datum
                            delnica.closeValue = value
                            delnica.save()
                    if code == '0020':
                        symbol = line[16:24].lstrip().rstrip()
                        closePrice = line[253:268].lstrip().rstrip().replace(',', '.')
                        if closePrice == '': continue
                        closePrice = float(closePrice)
                        try:
                            delnica = Delnica.objects.get(simbol=symbol)
                            if delnica.date == datum.date():
                                continue
                            delnica.date = datum
                            delnica.closeValue = closePrice
                            delnica._history_date = datum
                            delnica.save()
                        except Delnica.DoesNotExist:
                            delnica = Delnica(simbol=symbol)
                            delnica.date = datum
                            delnica.closeValue = closePrice
                            delnica._history_date = datum
                            delnica.save()
            file.close()

        makeDelnice(fileName)